package tradingbot.model;

public class StochOscillator {

  private String kLine;
  private String dLine;

  public String getkLine() {
    return kLine;
  }

  public void setkLine(String kLine) {
    this.kLine = kLine;
  }

  public String getdLine() {
    return dLine;
  }

  public void setdLine(String dLine) {
    this.dLine = dLine;
  }
}
