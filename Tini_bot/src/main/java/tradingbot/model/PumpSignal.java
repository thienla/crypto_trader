package tradingbot.model;

import com.binance.api.client.domain.market.Candlestick;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;
import java.util.Map;

public class PumpSignal {
    String symbol;
    long dateTime;
    String currentPrice;
    String volume;
    String quoteVolume;
    Map<String,List<String>> rsiTimeFrames;
    Map<String,List<String>> stochRsiTimeFrames;
    Map<String,List<StochOscillator>> stochOscTimeFrames;
    Map<String,List<Candlestick>> closeTimeFrames;
    Map<String,List<Macd>> histogramTimeFrames;
    Map<String,List<String>> ma5TimeFrames;
    Map<String,List<String>> ma9TimeFrames;
    Map<String,List<String>> ma13TimeFrames;
    Map<String,List<String>> ma20TimeFrames;
    Map<String,List<String>> ma50TimeFrames;
    Map<String,List<String>> ma100TimeFrames;
    Map<String,Double> recentBuyVolume;
    Map<String,Double> recentSellVolume;
    Map<String,Double> recentBuySymbolVolume;
    Map<String,Double> recentSellSymbolVolume;
    Map<String,String> futurePrices;


    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public long getDateTime() {
        return dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    public String getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(String currentPrice) {
        this.currentPrice = currentPrice;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getQuoteVolume() {
        return quoteVolume;
    }

    public void setQuoteVolume(String quoteVolume) {
        this.quoteVolume = quoteVolume;
    }

    public Map<String, List<String>> getRsiTimeFrames() {
        return rsiTimeFrames;
    }

    public void setRsiTimeFrames(Map<String, List<String>> rsiTimeFrames) {
        this.rsiTimeFrames = rsiTimeFrames;
    }

    public Map<String, List<Candlestick>> getCloseTimeFrames() {
        return closeTimeFrames;
    }

    public void setCloseTimeFrames(Map<String, List<Candlestick>> closeTimeFrames) {
        this.closeTimeFrames = closeTimeFrames;
    }

    public Map<String, List<Macd>> getHistogramTimeFrames() {
        return histogramTimeFrames;
    }

    public void setHistogramTimeFrames(Map<String, List<Macd>> histogramTimeFrames) {
        this.histogramTimeFrames = histogramTimeFrames;
    }

    public Map<String, Double> getRecentBuyVolume() {
        return recentBuyVolume;
    }

    public void setRecentBuyVolume(Map<String, Double> recentBuyVolume) {
        this.recentBuyVolume = recentBuyVolume;
    }

    public Map<String, Double> getRecentSellVolume() {
        return recentSellVolume;
    }

    public void setRecentSellVolume(Map<String, Double> recentSellVolume) {
        this.recentSellVolume = recentSellVolume;
    }

//    public List<Map<String, String>> getRecentTrades() {
//        return recentTrades;
//    }
//
//    public void setRecentTrades(List<Map<String, String>> recentTrades) {
//        this.recentTrades = recentTrades;
//    }

    public Map<String, String> getFuturePrices() {
        return futurePrices;
    }

    public void setFuturePrices(Map<String, String> futurePrices) {
        this.futurePrices = futurePrices;
    }

    public Map<String, List<String>> getMa20TimeFrames() {
        return ma20TimeFrames;
    }

    public void setMa20TimeFrames(Map<String, List<String>> ma20TimeFrames) {
        this.ma20TimeFrames = ma20TimeFrames;
    }

    public Map<String, List<String>> getMa50TimeFrames() {
        return ma50TimeFrames;
    }

    public void setMa50TimeFrames(Map<String, List<String>> ma50TimeFrames) {
        this.ma50TimeFrames = ma50TimeFrames;
    }

    public Map<String, List<String>> getMa100TimeFrames() {
        return ma100TimeFrames;
    }

    public void setMa100TimeFrames(Map<String, List<String>> ma100TimeFrames) {
        this.ma100TimeFrames = ma100TimeFrames;
    }

    public Map<String, List<String>> getStochRsiTimeFrames() {
        return stochRsiTimeFrames;
    }

    public void setStochRsiTimeFrames(Map<String, List<String>> stochRsiTimeFrames) {
        this.stochRsiTimeFrames = stochRsiTimeFrames;
    }

  public Map<String, List<String>> getMa5TimeFrames() {
    return ma5TimeFrames;
  }

  public void setMa5TimeFrames(Map<String, List<String>> ma5TimeFrames) {
    this.ma5TimeFrames = ma5TimeFrames;
  }

  public Map<String, List<String>> getMa9TimeFrames() {
    return ma9TimeFrames;
  }

  public void setMa9TimeFrames(Map<String, List<String>> ma9TimeFrames) {
    this.ma9TimeFrames = ma9TimeFrames;
  }

  public Map<String, List<String>> getMa13TimeFrames() {
    return ma13TimeFrames;
  }

  public void setMa13TimeFrames(Map<String, List<String>> ma13TimeFrames) {
    this.ma13TimeFrames = ma13TimeFrames;
  }

    public Map<String, List<StochOscillator>> getStochOscTimeFrames() {
        return stochOscTimeFrames;
    }

    public void setStochOscTimeFrames(Map<String, List<StochOscillator>> stochOscTimeFrames) {
        this.stochOscTimeFrames = stochOscTimeFrames;
    }

    public Map<String, Double> getRecentBuySymbolVolume() {
        return recentBuySymbolVolume;
    }

    public void setRecentBuySymbolVolume(Map<String, Double> recentBuySymbolVolume) {
        this.recentBuySymbolVolume = recentBuySymbolVolume;
    }

    public Map<String, Double> getRecentSellSymbolVolume() {
        return recentSellSymbolVolume;
    }

    public void setRecentSellSymbolVolume(Map<String, Double> recentSellSymbolVolume) {
        this.recentSellSymbolVolume = recentSellSymbolVolume;
    }

    @Override
    public String toString() {
        return "PumpSignal{" +
                "symbol='" + symbol + '\'' +
                ", dateTime=" + dateTime +
                ", currentPrice='" + currentPrice + '\'' +
                ", volume='" + volume + '\'' +
                ", quoteVolume='" + quoteVolume + '\'' +
                ", rsiTimeFrames=" + rsiTimeFrames +
                ", closeTimeFrames=" + closeTimeFrames +
                ", histogramTimeFrames=" + histogramTimeFrames +
                ", ma20TimeFrames=" + ma20TimeFrames +
                ", ma50TimeFrames=" + ma50TimeFrames +
                ", ma100TimeFrames=" + ma100TimeFrames +
                ", recentBuyVolume=" + recentBuyVolume +
                ", recentSellVolume=" + recentSellVolume +
                ", futurePrices=" + futurePrices +
                '}';
    }

    public String toJson() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return  mapper.writeValueAsString(this);
    }
}
