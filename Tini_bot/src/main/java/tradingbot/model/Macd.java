package tradingbot.model;

public class Macd {
  private String macdLine;
  private String signalLine;
  private String histogram;

  public String getMacdLine() {
    return macdLine;
  }

  public void setMacdLine(String macdLine) {
    this.macdLine = macdLine;
  }

  public String getSignalLine() {
    return signalLine;
  }

  public void setSignalLine(String signalLine) {
    this.signalLine = signalLine;
  }

  public String getHistogram() {
    return histogram;
  }

  public void setHistogram(String histogram) {
    this.histogram = histogram;
  }
}
