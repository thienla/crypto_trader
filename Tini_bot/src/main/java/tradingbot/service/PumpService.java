package tradingbot.service;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.*;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import org.springframework.stereotype.Service;
import tradingbot.model.PumpSignal;

import java.io.InputStream;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Service
public class PumpService {
  Firestore db = null;
  CollectionReference bumpCollection;
  CollectionReference bumpLearningCollection;

  public PumpService() {
    InputStream serviceAccount = null;
    try {
      serviceAccount = PumpService.class.getResourceAsStream("/kubi-bot-key.json");
      GoogleCredentials credentials = GoogleCredentials.fromStream(serviceAccount);
      FirebaseOptions options = new FirebaseOptions.Builder()
          .setCredentials(credentials)
          .build();
      FirebaseApp firebaseApp = FirebaseApp.initializeApp(options, "kubi_bot");
      db = FirestoreClient.getFirestore(firebaseApp);
      bumpCollection = db.collection("kubi_data");
      bumpLearningCollection = db.collection("tini_data");
    } catch (java.io.IOException e) {
      e.printStackTrace();
    }
  }

  public PumpSignal getPumpSignalById(String id) throws ExecutionException, InterruptedException {
    DocumentReference pump = bumpCollection.document(id);
    return pump.get().get().toObject(PumpSignal.class);
  }

  public PumpSignal getPumpSignalBySymbolAndTime(String symbol,long time) throws ExecutionException, InterruptedException {
    DocumentReference pump = bumpCollection.whereEqualTo("symbol",symbol).whereEqualTo("dateTime",time).get().get().getDocumentChanges().get(0).getDocument().getReference();
    return pump.get().get().toObject(PumpSignal.class);
  }

  public boolean isPumpSignalExisted(String symbol,long time) throws ExecutionException, InterruptedException {
    return bumpLearningCollection.whereEqualTo("symbol",symbol).whereEqualTo("dateTime",time).get().get().getDocuments().size() > 0;
  }

  public List<PumpSignal> getPumpSignals(long fromTime, int limit) throws ExecutionException, InterruptedException {
    List<QueryDocumentSnapshot> documents = bumpCollection.whereLessThan("dateTime", fromTime).get().get().getDocuments();
    List<PumpSignal> pumpSignals = documents.stream().map(queryDocumentSnapshot -> {
          PumpSignal pumpSignal = null;
          try {
            pumpSignal = queryDocumentSnapshot.toObject(PumpSignal.class);
          } catch (Exception e) {
            pumpSignal = new PumpSignal();
            pumpSignal.setDateTime(0);
          }
          return pumpSignal;
        }
    ).filter(pumpSignal -> pumpSignal.getDateTime()!=0).collect(Collectors.toList());
    return pumpSignals;
  }

  public void savePumpSignals(PumpSignal pumpSignal) throws ExecutionException, InterruptedException {
    bumpLearningCollection.document().create(pumpSignal).get().getUpdateTime();
  }

  public void updatePumpSignals(PumpSignal pumpSignal) throws ExecutionException, InterruptedException {
    bumpLearningCollection.whereEqualTo("symbol",pumpSignal.getSymbol()).whereEqualTo("dateTime",pumpSignal.getDateTime()).get().get().getDocumentChanges().get(0).getDocument().getReference().set(pumpSignal).get().getUpdateTime();
  }

  public void savePumpSignals(List<PumpSignal> pumpSignals) throws ExecutionException, InterruptedException {
    WriteBatch batch = db.batch();
    for (PumpSignal p : pumpSignals) {
      batch.create(bumpLearningCollection.document(), p);
    }
    ApiFuture<List<WriteResult>> future = batch.commit();
    for (WriteResult result : future.get()) {
      System.out.println("Create time : " + result.getUpdateTime());
    }
  }
}
