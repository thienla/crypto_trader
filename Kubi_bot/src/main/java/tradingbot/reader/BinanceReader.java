package tradingbot.reader;

import com.binance.api.client.BinanceApiCallback;
import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.BinanceApiWebSocketClient;
import com.binance.api.client.domain.event.AggTradeEvent;
import com.binance.api.client.domain.event.CandlestickEvent;
import com.binance.api.client.domain.market.Candlestick;
import com.binance.api.client.domain.market.CandlestickInterval;
import com.binance.api.client.domain.market.TickerStatistics;
import com.binance.api.client.impl.BinanceApiWebSocketClientImpl;
import org.springframework.scheduling.annotation.Scheduled;
import org.ta4j.core.*;
import org.ta4j.core.indicators.EMAIndicator;
import org.ta4j.core.indicators.SMAIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import tradingbot.TradingBotApplication;
import tradingbot.planer.indicator.CloseVolumeIndicator;
import tradingbot.planer.model.CandleBar;
import tradingbot.planer.rule.FomoRule;
import tradingbot.worker.AlertPumpSender;

import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by truongnhukhang on 3/21/18.
 */
public class BinanceReader implements ExchangeReader {
  BinanceApiWebSocketClient client = BinanceApiClientFactory.newInstance().newWebSocketClient();
  BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance("API-KEY", "SECRET");
  BinanceApiRestClient clientRest = factory.newRestClient();
  TimeSeries timeSeries;
  FomoRule fomoRule;
  Set<Long> pumpTime = new HashSet<>();
  public boolean firstResponse = false;
  Closeable socketClose = null;
  Date startTime = null;
  boolean isRunning = false;
  //    AlertPumpSender alertPumpSender = new AlertPumpSender();

  private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");


  public boolean closeSocketAfter6h() {
    try {
      long timeToClose = startTime.toInstant().plus(6, ChronoUnit.HOURS).toEpochMilli();
      if(timeToClose <= System.currentTimeMillis()) {
        System.out.println("Close socket after 4h");
        socketClose.close();
        System.out.println("Socket close successfully");
        return true;
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return false;
  }

  public BinanceReader(TimeSeries timeSeries) {
    this.timeSeries = timeSeries;

  }


  @Override
  public void readBarFromExchange(String code, int candleTime) {
    if(!isRunning) {
      System.out.println("Start Watching " + code);
      CloseVolumeIndicator closeVolumeIndicator = new CloseVolumeIndicator(timeSeries);
      SMAIndicator sma50Indicator = new SMAIndicator(closeVolumeIndicator, 50);
      SMAIndicator sma3Indicator = new SMAIndicator(closeVolumeIndicator, 3);
      fomoRule = new FomoRule(sma50Indicator, sma3Indicator);
      if(timeSeries.isEmpty()) {
        try {
          System.out.println("Get first 50 bar : " + code);
          List<Candlestick> first50Candle = clientRest.getCandlestickBars(code, CandlestickInterval.ONE_MINUTE, 50, null, new Date().getTime());
          first50Candle.forEach(candlestick -> {
            CandleBar bar = new CandleBar(Duration.ofMinutes(candleTime),
                Instant.ofEpochMilli(candlestick.getOpenTime()+60000).atZone(ZoneOffset.UTC),
                Decimal.valueOf(candlestick.getOpen()),
                Decimal.valueOf(candlestick.getHigh()),
                Decimal.valueOf(candlestick.getLow()),
                Decimal.valueOf(candlestick.getClose()),
                Decimal.valueOf(candlestick.getVolume()));
            timeSeries.addBar(bar);

          });
        } catch (Exception e) {
          System.out.println("Error when try to get first 50 bar : " + code + " - Error : " + e.getMessage());
        }
      }

      System.out.println("Connect websocket : " + code);
      socketClose = client.onCandlestickEvent(code.toLowerCase(), CandlestickInterval.ONE_MINUTE, new BinanceApiCallback<CandlestickEvent>() {

        @Override
        public void onResponse(CandlestickEvent response) {
          if(!firstResponse) {
            System.out.println("Code " + code + " : " + response.toString());
            firstResponse = true;
          }
          try {
            if (response.getOpenTime().compareTo(timeSeries.getLastBar().getBeginTime().toInstant().toEpochMilli())>0) {
              CandleBar bar = new CandleBar(Duration.ofMinutes(candleTime),
                  Instant.ofEpochMilli(response.getOpenTime()+60000).atZone(ZoneOffset.UTC),
                  Decimal.valueOf(response.getOpen()),
                  Decimal.valueOf(response.getHigh()),
                  Decimal.valueOf(response.getLow()),
                  Decimal.valueOf(response.getClose()),
                  Decimal.valueOf(response.getVolume()));
              timeSeries.addBar(bar);
              if (fomoRule.isSatisfied(timeSeries.getEndIndex(), null)) {
                if(isCandleStillNotAlerted(response)) {
                  TickerStatistics statistics = clientRest.get24HrPriceStatistics(code);
                  TradingBotApplication.alertPumpSender.send(code, response, statistics.getVolume(), statistics.getQuoteVolume());
                }
              }
            } else {
              CandleBar bar = (CandleBar) timeSeries.getLastBar();
              convertResponeToBaseBar(response,bar);
              if (fomoRule.isSatisfied(timeSeries.getEndIndex(), null)) {
                if(isCandleStillNotAlerted(response)) {
                  TickerStatistics statistics = clientRest.get24HrPriceStatistics(code);
                  TradingBotApplication.alertPumpSender.send(code, response, statistics.getVolume(), statistics.getQuoteVolume());
                }
              }
            }
          } catch (Exception e) {
            System.err.println("Web socket failed : " + code + " - Reason : " + e.getMessage());
            try {
              socketClose.close();
            }  catch (IOException e1) {
              e1.printStackTrace();
            }
          }
        }

        @Override
        public void onFailure(Throwable cause) {
          System.err.println("Web socket failed : " + code + " - Reason : " + cause.getMessage());
          try {
            socketClose.close();
          }  catch (IOException e) {
            e.printStackTrace();
          }
        }

        @Override
        public void onClose() {
          System.out.println("Close socket");
          try {
            isRunning = false;
            TradingBotApplication.deadThread.put(code);
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      });
      startTime = new Date();
      isRunning = true;
    } else {
      System.out.println(code + "Already Running");
    }

  }

  private boolean isCandleStillNotAlerted(CandlestickEvent response) {
    return pumpTime.add(response.getOpenTime());
  }

  private void convertResponeToBaseBar(CandlestickEvent response, CandleBar candleBar) {
    candleBar.setMaxPrice(Decimal.valueOf(response.getHigh()));
    candleBar.setMinPrice(Decimal.valueOf(response.getLow()));
    candleBar.setClosePrice(Decimal.valueOf(response.getClose()));
    candleBar.setVolume(Decimal.valueOf(response.getVolume()));
  }

  @Override
  public void readBarFromJsonfile(String location) {

  }
}
