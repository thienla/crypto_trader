package tradingbot.planer.strategy;

import org.ta4j.core.*;
import org.ta4j.core.indicators.EMAIndicator;
import org.ta4j.core.indicators.SMAIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.indicators.helpers.OpenPriceIndicator;
import org.ta4j.core.trading.rules.IsRisingRule;
import org.ta4j.core.trading.rules.StopGainRule;
import org.ta4j.core.trading.rules.StopLossRule;
import tradingbot.planer.indicator.CloseVolumeIndicator;
import tradingbot.planer.indicator.MaxPriceIndicator;
import tradingbot.planer.rule.FomoRule;

/**
 * Created by khangtn on 3/24/2018.
 */
public class FomoStrategy {

  public static Strategy buildStrategy(TimeSeries timeSeries) {
    if(timeSeries==null) {
      throw new IllegalArgumentException("Serries can't be null");
    }

    ClosePriceIndicator closePriceIndicator = new ClosePriceIndicator(timeSeries);
    CloseVolumeIndicator closeVolumeIndicator = new CloseVolumeIndicator(timeSeries);
    SMAIndicator emaIndicator_50 = new SMAIndicator(closeVolumeIndicator,50);
    SMAIndicator emaIndicator_3 = new SMAIndicator(closeVolumeIndicator,3);
    Rule entryRule = new FomoRule(emaIndicator_50,emaIndicator_3);
    Rule exitRule = new StopLossRule(closePriceIndicator, Decimal.valueOf(2)).or(new StopGainRule(closePriceIndicator,Decimal.valueOf(20)));

    return new BaseStrategy(entryRule,exitRule);
  }


}
