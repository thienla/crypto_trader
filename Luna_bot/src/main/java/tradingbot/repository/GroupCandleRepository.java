package tradingbot.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.annotation.Depth;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;
import tradingbot.model.GroupCandle;

import java.util.List;

@Repository
public interface GroupCandleRepository extends Neo4jRepository<GroupCandle,Long> {
  List<GroupCandle> findGroupCandlesBySymbolInAndAndOpenTimeIn(List<String> symbols, List<Long> openTimes, @Depth int depth);
  Long countAllBySymbolInAndAndOpenTimeIn(List<String> symbols, List<Long> openTimes, @Depth int depth);
  GroupCandle findGroupCandleBySymbolAndOpenTime(String symbol, long opentime, @Depth int depth);
  //match(g:GroupCandle)-[h30:HAVE_30]-(c30:Candle)-[hs30:HAVE]->(s30:Signal) with g,h30,c30,hs30,s30,count(hs30) as numberHS30 Match(g2:GroupCandle)-[h60:HAVE_60]-(c60:Candle)-[hs60:HAVE]->(s60:Signal) where g.symbol='TRXBTC'and g2.symbol=g.symbol and g.openTime=g2.openTime and s30.type in ['Histogram Bull divergence','MACD Bull cross','MACDLine Bull divergence','Candle in MA5','MA5 ALMOST_CROSS MA9','Rsi Bull divergence']  and s60.type in ['Histogram Bull divergence','MACD Bull cross','MACDLine Bull divergence','Candle in MA5','MA5 ALMOST_CROSS MA9','Rsi Bull divergence'] return g,h30,c30,hs30,s30,h60,c60,hs60,s60, count(hs60)*2 + numberHS30 as score order by score DESC
  @Query(value = "Match(g:GroupCandle)-[:HAVE_30]-(:Candle)-[hs30:HAVE]->(s30:Signal) with g,s30,count(s30) as numberHS30 " +
      "Match(g)-[:HAVE_60]-(:Candle)-[hs60:HAVE]->(s60:Signal) with g,s60,s30,numberHS30,count(hs60) as numberHS60 " +
      "Match(g)-[:HAVE_120]-(:Candle)-[hs120:HAVE]->(s120:Signal) with g,s60,s30,s120,numberHS30,numberHS60,count(hs120) as numberHS120 " +
      "Match(g)-[:HAVE_240]-(:Candle)-[hs240:HAVE]->(s240:Signal) with g,s60,s30,s120,s240,numberHS30,numberHS60,numberHS120,count(hs240) as numberHS240 " +
      "Match(g)-[:HAVE_1440]-(:Candle)-[hs1440:HAVE]->(s1440:Signal) with g,s60,s30,s120,s240,s1440,numberHS30,numberHS60,numberHS120,numberHS240,count(hs1440) as numberHS1440 " +
      "WHERE s30.type in {0} or s60.type in {1} or s120.type in {2} or s240.type in {3} or s1440.type in {4} " +
      "Return g,(numberHS30 * {5} + numberHS60 * {6} + numberHS120 * {7} + numberHS240 * {8} + numberHS1440 * {9}) as totalScore order by totalScore DESC",
  countQuery = "Match(g:GroupCandle)-[:HAVE_30]-(:Candle)-[hs30:HAVE]->(s30:Signal) with g,s30,count(hs30) as numberHS30 " +
      "Match(g)-[:HAVE_60]-(:Candle)-[hs60:HAVE]->(s60:Signal) with g,s60,s30,numberHS30,count(hs60) as numberHS60 " +
      "Match(g)-[:HAVE_120]-(:Candle)-[hs120:HAVE]->(s120:Signal) with g,s60,s30,s120,numberHS30,numberHS60,count(hs120) as numberHS120 " +
      "Match(g)-[:HAVE_240]-(:Candle)-[hs240:HAVE]->(s240:Signal) with g,s60,s30,s120,s240,numberHS30,numberHS60,numberHS120,count(hs240) as numberHS240 " +
      "Match(g)-[:HAVE_1440]-(:Candle)-[hs1440:HAVE]->(s1440:Signal) with g,s60,s30,s120,s240,s1440,numberHS30,numberHS60,numberHS120,numberHS240,count(hs1440) as numberHS1440 " +
      "WHERE s30.type in {0} or s60.type in {1} or s120.type in {2} or s240.type in {3} or s1440.type in {4} " +
      "Return count(g)")
  Page<GroupCandle> findGroupCandlesBySimilarSignal(
      List<String> s30Signals, List<String> s60Signals, List<String> s120Signals, List<String> s240Signals, List<String> s1440Signals,
      Integer score_30, Integer score_60, Integer score_120, Integer score_240, Integer score_1440, Pageable pageable
  );
}
