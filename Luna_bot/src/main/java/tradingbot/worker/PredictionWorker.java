package tradingbot.worker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import tradingbot.SignalType;
import tradingbot.model.GroupCandle;
import tradingbot.model.SignalPrediction;
import tradingbot.model.SignalPredictionLearning;
import tradingbot.repository.FireStoreRepository;
import tradingbot.repository.GroupCandleRepository;
import tradingbot.repository.GroupCandleSearchRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by truongnhukhang on 10/30/18.
 */
@Component
public class PredictionWorker {

  @Autowired
  GroupCandleSearchRepository groupCandleSearchRepository;

  @Autowired
  GroupCandleRepository groupCandleRepository;

  FireStoreRepository fireStoreRepository = new FireStoreRepository();

  @Scheduled(cron = "0 10 0/4 * * *")
  public void predict() {
    Date startTime = new Date();
    Long openTime = Long.valueOf("1529625600000");
    System.out.println("Start scoring : " + startTime);
    List<SignalPrediction> signalPredictions = null;
    try {
      signalPredictions = fireStoreRepository.getSignalPredictions();
    } catch (ExecutionException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    if (signalPredictions != null) {
      System.out.println("Sort List by Descending Volume : ");
      signalPredictions.sort((o1, o2) -> Double.compare(o2.getRecentBuyVolume().get("60"), o1.getRecentBuyVolume().get("60")));
      System.out.println("Finish sorting");
      signalPredictions.forEach(signalPrediction -> {
        List<String> signals_30 = signalPrediction.getBullishSignal().get("30");
        List<String> signals_60 = signalPrediction.getBullishSignal().get("60");
        List<String> signals_120 = signalPrediction.getBullishSignal().get("120");
        List<String> signals_240 = signalPrediction.getBullishSignal().get("240");
        List<String> signals_1440 = signalPrediction.getBullishSignal().get("1440");
        List<GroupCandle> groupCandles = new ArrayList<>();
        try {
          groupCandles = groupCandleSearchRepository
              .findGroupCandlesBySimilarSignal("and", signals_30, signals_60, signals_120, signals_240, signals_1440
                  , 1, 2, 3, 4, 5, 20, 0,openTime);

        } catch (Exception e) {
          e.printStackTrace();
        }
        if (groupCandles.size() > 0) {
          List<Long> ids = groupCandles.stream().map(GroupCandle::getId).collect(Collectors.toList());
          Iterable<GroupCandle> groupCandleIterable = groupCandleRepository.findAllById(ids, 1);
          Integer totalScore = getScoreFromBullishSignal(signalPrediction.getBullishSignal());
          groupCandles = new ArrayList<>();
          groupCandleIterable.forEach(groupCandles::add);
          float percentChangeAfter5min = groupCandles.stream().map(groupCandle -> groupCandle.getFuturePriceChangePercentBaseOnTotalScore(5, totalScore)).reduce(Float::sum).get();
          float percentChangeAfter10min = groupCandles.stream().map(groupCandle -> groupCandle.getFuturePriceChangePercentBaseOnTotalScore(10, totalScore)).reduce(Float::sum).get();
          float percentChangeAfter15min = groupCandles.stream().map(groupCandle -> groupCandle.getFuturePriceChangePercentBaseOnTotalScore(15, totalScore)).reduce(Float::sum).get();
          float percentChangeAfter30min = groupCandles.stream().map(groupCandle -> groupCandle.getFuturePriceChangePercentBaseOnTotalScore(30, totalScore)).reduce(Float::sum).get();
          float percentChangeAfter60min = groupCandles.stream().map(groupCandle -> groupCandle.getFuturePriceChangePercentBaseOnTotalScore(60, totalScore)).reduce(Float::sum).get();
          float percentChangeAfter120min = groupCandles.stream().map(groupCandle -> groupCandle.getFuturePriceChangePercentBaseOnTotalScore(120, totalScore)).reduce(Float::sum).get();
          float percentChangeAfter240min = groupCandles.stream().map(groupCandle -> groupCandle.getFuturePriceChangePercentBaseOnTotalScore(240, totalScore)).reduce(Float::sum).get();
          float percentChangeAfter480min = groupCandles.stream().map(groupCandle -> groupCandle.getFuturePriceChangePercentBaseOnTotalScore(480, totalScore)).reduce(Float::sum).get();
          float percentChangeAfter1440min = groupCandles.stream().map(groupCandle -> groupCandle.getFuturePriceChangePercentBaseOnTotalScore(1440, totalScore)).reduce(Float::sum).get();
          float percentChangeAfter2480min = groupCandles.stream().map(groupCandle -> groupCandle.getFuturePriceChangePercentBaseOnTotalScore(2480, totalScore)).reduce(Float::sum).get();
          signalPrediction.setPriceAfter5min(signalPrediction.getCurrentPrice() + signalPrediction.getCurrentPrice() * percentChangeAfter5min / 100);
          signalPrediction.setPriceAfter10min(signalPrediction.getCurrentPrice() + signalPrediction.getCurrentPrice() * percentChangeAfter10min / 100);
          signalPrediction.setPriceAfter15min(signalPrediction.getCurrentPrice() + signalPrediction.getCurrentPrice() * percentChangeAfter15min / 100);
          signalPrediction.setPriceAfter30min(signalPrediction.getCurrentPrice() + signalPrediction.getCurrentPrice() * percentChangeAfter30min / 100);
          signalPrediction.setPriceAfter60min(signalPrediction.getCurrentPrice() + signalPrediction.getCurrentPrice() * percentChangeAfter60min / 100);
          signalPrediction.setPriceAfter120min(signalPrediction.getCurrentPrice() + signalPrediction.getCurrentPrice() * percentChangeAfter120min / 100);
          signalPrediction.setPriceAfter240min(signalPrediction.getCurrentPrice() + signalPrediction.getCurrentPrice() * percentChangeAfter240min / 100);
          signalPrediction.setPriceAfter480min(signalPrediction.getCurrentPrice() + signalPrediction.getCurrentPrice() * percentChangeAfter480min / 100);
          signalPrediction.setPriceAfter1440min(signalPrediction.getCurrentPrice() + signalPrediction.getCurrentPrice() * percentChangeAfter1440min / 100);
          signalPrediction.setPriceAfter2480min(signalPrediction.getCurrentPrice() + signalPrediction.getCurrentPrice() * percentChangeAfter2480min / 100);
          try {
            fireStoreRepository.savePrediction(signalPrediction);
            SignalPredictionLearning signalPredictionLearning = new SignalPredictionLearning(signalPrediction);
            signalPredictionLearning.setAlgorithm("basic");
            fireStoreRepository.saveLearningPrediction(signalPredictionLearning);
          } catch (ExecutionException e) {
            e.printStackTrace();
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        } else {
          signalPrediction.setPriceAfter5min(0);
          signalPrediction.setPriceAfter10min(0);
          signalPrediction.setPriceAfter15min(0);
          signalPrediction.setPriceAfter30min(0);
          signalPrediction.setPriceAfter60min(0);
          signalPrediction.setPriceAfter120min(0);
          signalPrediction.setPriceAfter240min(0);
          signalPrediction.setPriceAfter480min(0);
          signalPrediction.setPriceAfter1440min(0);
          signalPrediction.setPriceAfter2480min(0);
          try {
            fireStoreRepository.savePrediction(signalPrediction);
          } catch (ExecutionException e) {
            e.printStackTrace();
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      });
      Date endTime = new Date();
      System.out.println("End scoring : " + endTime + " | it took : " + (endTime.getTime() - startTime.getTime()) / 1000 / 60 + " minutes");

    }
  }

  public Integer getScoreFromBullishSignal(Map<String, List<String>> bullishSignal) {
    int score_30 = 0;
    int score_60 = 0;
    int score_120 = 0;
    int score_240 = 0;
    int score_1440 = 0;
    if (bullishSignal.get("30") != null) {
      score_30 = bullishSignal.get("30").stream().map(mapSignalStringToTheirScore()).reduce(Integer::sum).get();
    }
    if (bullishSignal.get("60") != null) {
      score_60 = bullishSignal.get("60").stream().map(mapSignalStringToTheirScore()).reduce(Integer::sum).get();
    }
    if (bullishSignal.get("120") != null) {
      score_120 = bullishSignal.get("120").stream().map(mapSignalStringToTheirScore()).reduce(Integer::sum).get();
    }
    if (bullishSignal.get("240") != null) {
      score_240 = bullishSignal.get("240").stream().map(mapSignalStringToTheirScore()).reduce(Integer::sum).get();
    }
    if (bullishSignal.get("1440") != null) {
      score_1440 = bullishSignal.get("1440").stream().map(mapSignalStringToTheirScore()).reduce(Integer::sum).get();
    }
    return score_30 + score_60 * 2 + score_120 * 3 + score_240 * 4 + score_1440 * 5;
  }

  private Function<String, Integer> mapSignalStringToTheirScore() {
    return s -> {
      return Stream.of(SignalType.values()).filter(signalType -> signalType.toString().equals(s)).findFirst().get().getScore();
    };
  }
}
