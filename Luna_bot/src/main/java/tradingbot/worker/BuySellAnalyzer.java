package tradingbot.worker;

import com.binance.api.client.domain.event.CandlestickEvent;
import com.binance.api.client.domain.market.AggTrade;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import org.ta4j.core.Decimal;
import tradingbot.model.BuySellLine;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class BuySellAnalyzer {
    Firestore db = null;
    CollectionReference bumpCollection;
    public static final String History_Location = "C:\\historyCandle_";
    public static final String History_Location_MAC = "/Users/truongnhukhang/work/historyCandle_";

    public BuySellAnalyzer(String symbol) {
//        InputStream serviceAccount = null;
//        try {
//            serviceAccount = FireStoreRepository.class.getResourceAsStream("/kubi-bot-key.json");
//            GoogleCredentials credentials = GoogleCredentials.fromStream(serviceAccount);
//            FirebaseOptions options = new FirebaseOptions.Builder()
//                    .setCredentials(credentials)
//                    .build();
//            FirebaseApp firebaseApp = FirebaseApp.initializeApp(options,"kubi_bot");
//            db = FirestoreClient.getFirestore(firebaseApp);
//            bumpCollection = db.collection(symbol+"_"+"trading_data");
//        } catch (java.io.IOException e) {
//            e.printStackTrace();
//        }

    }

    public void send(long startTime,long Endtime,String candleStick,long btcIn,long btcOut) {
        Map<String,Object> data = new HashMap<>();

        bumpCollection.add(data);
    }

    public void covertToLine(String historyDataLocation,long startTime,long endTime,int candlestickInterval) {
        ObjectMapper mapper = new ObjectMapper();
        long candlestickIntervalMilisecond = candlestickInterval * 60000;
        List<BuySellLine> buySellLines = new ArrayList<>();

        for (long i = startTime; i <= endTime; i=i+candlestickIntervalMilisecond) {
            buySellLines.add(new BuySellLine(candlestickInterval,i,i+candlestickIntervalMilisecond));
        }
        try {
            List<AggTrade> aggTradeList = mapper.readValue(new File(historyDataLocation),new TypeReference<List<AggTrade>>(){});
            for (int i = 0; i < aggTradeList.size(); i++) {
                AggTrade aggTrade = aggTradeList.get(i);
                BuySellLine tradeLine = null;
                try {
                    for(BuySellLine buySellLine : buySellLines) {
                        if(buySellLine.getStartTime() <= aggTrade.getTradeTime() && buySellLine.getEndTime() >= aggTrade.getTradeTime()) {
                            tradeLine = buySellLine;
                        }
                    }
                } catch (Exception e) {
                    System.out.println(aggTrade.getTradeTime());
                }
                double btc = Decimal.valueOf(aggTrade.getPrice()).multipliedBy(Decimal.valueOf(aggTrade.getQuantity())).doubleValue();
                if(aggTrade.isBuyerMaker()) {
                    tradeLine.setNumberBTC_out(tradeLine.getNumberBTC_out()+btc);
                    Map<String,Double> sellVolume = tradeLine.getSellVolume();
                    if(sellVolume.get(aggTrade.getPrice())!=null) {
                        Double vol = sellVolume.get(aggTrade.getPrice())+btc;
                        sellVolume.put(aggTrade.getPrice(),vol);
                    } else {
                        sellVolume.put(aggTrade.getPrice(),btc);
                    }
                } else {
                    tradeLine.setNumberBTC_In(tradeLine.getNumberBTC_In()+btc);
                    Map<String,Double> buyVolume = tradeLine.getBuyVolume();
                    if(buyVolume.get(aggTrade.getPrice())!=null) {
                        Double vol = buyVolume.get(aggTrade.getPrice())+btc;
                        buyVolume.put(aggTrade.getPrice(),vol);
                    } else {
                        buyVolume.put(aggTrade.getPrice(),btc);
                    }
                }
            }
            buySellLines = buySellLines.stream().filter(line -> line.getNumberBTC_out()!=0 || line.getNumberBTC_In()!=0).collect(Collectors.toList());
            buySellLines.forEach(buySellLine -> {
                Map<String,Double> buyVolume = buySellLine.getBuyVolume();
                Map<String,Double> sellVolume = buySellLine.getSellVolume();
                buySellLine.setBuyVolume(BuySellAnalyzer.sortByValue(buyVolume));
                buySellLine.setSellVolume(BuySellAnalyzer.sortByValue(sellVolume));
            });
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date startDate = new Date(startTime);
            Date endDate = new Date(endTime);
            mapper.writerWithDefaultPrettyPrinter().writeValue(new File(History_Location  +"LINE_TRADE_HISTORY_" + df.format(startDate) + "_" + df.format(endDate)+".json")
                    ,buySellLines);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> unsortMap) {

        List<Map.Entry<K, V>> list =
                new LinkedList<Map.Entry<K, V>>(unsortMap.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        Map<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;

    }
}
