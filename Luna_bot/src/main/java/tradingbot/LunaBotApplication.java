package tradingbot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import tradingbot.model.SignalPredictionLearning;
import tradingbot.repository.FireStoreRepository;
import tradingbot.worker.AnalyzerWorker;
import tradingbot.worker.LearningResultWorker;
import tradingbot.worker.PredictionWorker;

import java.util.concurrent.ExecutionException;

@SpringBootApplication
@EnableAsync
@EnableScheduling
public class LunaBotApplication implements CommandLineRunner {


  @Autowired
  AnalyzerWorker analyzerWorker;

  @Autowired
  PredictionWorker predictionWorker;

  public static void main(String[] args) {
    SpringApplication springApplication =
        new SpringApplicationBuilder()
            .sources(LunaBotApplication.class)
            .web(false)
            .build();

    springApplication.run(LunaBotApplication.class, args);


  }

  @Override
  public void run(String... strings) throws Exception {
    analyzerWorker.prepareAnalyze();
//    analyzerWorker.updateTAJob();
//    predictionWorker.predict();
//    LearningResultWorker learningResultWorker = new LearningResultWorker();
//    learningResultWorker.generateLearningResult();

  }
//		System.out.println("DeadThread pool : " + deadThread.size());
//		System.out.println("Start DeadThread bot ");
//		Thread recoverThread = new Thread(()->{
//			while (true) {
//				try {
//					Thread.sleep(5000);
//				} catch (InterruptedException e) {
//					e.printStackTrace();
//				}
//				if(!deadThread.isEmpty()) {
//					Map<String,Integer> signal = deadThread.poll();
//					System.out.println("Recover thread : " + signal);
//					String symbol = signal.keySet().iterator().next();
//					Integer timeFrame = signal.get(symbol);
//					if(timeFrame!=0) {
//						TimeSeries recoverSeries = new BaseTimeSeries();
//						recoverSeries.setMaximumBarCount(100);
//						ExchangeReader exchangeReader = exchangeReaderMap.get(symbol+"_"+timeFrame);
//						exchangeReader.readBarFromExchange(symbol,timeFrame);
//					} else {
//						SignalPrediction signalPrediction = signalPredictions.stream().filter(sp -> sp.getSymbol().equals(symbol)).findFirst().get();
//						createBuySellData(signalPrediction);
//					}
//
//				}
//			}
//		});
//		recoverThread.start();
//		System.out.println("Start close thread");
//		Thread closeThread = new Thread(()->{
//			boolean isRunning = true;
//			while (isRunning) {
//				try {
//					Thread.sleep(60000);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//				exchangeReaderMap.keySet().forEach(key -> {
//					BinanceReader binanceReader = (BinanceReader) exchangeReaderMap.get(key);
//					binanceReader.closeSocketAfter6h();
//				});
//
//			}
//		});
//		closeThread.start();

}
