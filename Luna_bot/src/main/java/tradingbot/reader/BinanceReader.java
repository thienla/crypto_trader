package tradingbot.reader;

import com.binance.api.client.BinanceApiCallback;
import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.BinanceApiWebSocketClient;
import com.binance.api.client.domain.event.CandlestickEvent;
import com.binance.api.client.domain.market.Candlestick;
import com.binance.api.client.domain.market.CandlestickInterval;
import org.ta4j.core.BaseTimeSeries;
import org.ta4j.core.Decimal;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.MACDIndicator;
import org.ta4j.core.indicators.RSIIndicator;
import org.ta4j.core.indicators.SMAIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.indicators.helpers.VolumeIndicator;
import tradingbot.LunaBotApplication;
import tradingbot.SignalType;
import tradingbot.indicator.MACDHistogramIndicator;
import tradingbot.model.CandleBar;
import tradingbot.model.SignalPrediction;
import tradingbot.rule.*;

import java.io.Closeable;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * Created by truongnhukhang on 3/21/18.
 */
public class BinanceReader implements ExchangeReader {
  BinanceApiWebSocketClient client = BinanceApiClientFactory.newInstance().newWebSocketClient();
  BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance("API-KEY", "SECRET");
  SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
  BinanceApiRestClient clientRest = factory.newRestClient();
  TimeSeries timeSeries;
  SignalPrediction signalPrediction;
  Closeable socketClose;
  CandleBullishInMA candleInMA5Rule;
  CandleBullishInMA candleInMA9Rule;
  CandleBullishInMA candleInMA20Rule;
  CandleBullishAboveMA candleAboveMA5Rule;
  CandleBullishAboveMA candleAboveMA9Rule;
  CandleBullishAboveMA candleAboveMA20Rule;
  RSIUnder30 rsiUnder30;
  MAAlmostCrossing ma5AlmossMa9Rule;
  MAAlmostCrossing ma5AlmossMa20Rule;
  MACrossEachOther ma5crossMA9Rule;
  MACrossEachOther ma5crossMA20Rule;
  MACrossEachOther ma9crossMA20Rule;
  RSIBullishDivergence rsiBullishDivergence;
  ThreeBullishCandleRule threeBullishCandleRule;
  MACDCross macdCrossRule;
  MACDHIstogramBullishDivergence macdhIstogramBullishDivergenceRule;
  MACDLineBullishDivergence macdLineBullishDivergenceRule;
  MAAlmostCrossing ma9AlmossMa20Rule;
  VolumeGreaterMA volumeGreaterMA50;
  Date startTime = null;
  boolean isRunning = false;

  public BinanceReader(TimeSeries timeSeries, SignalPrediction signalPrediction) {
    this.timeSeries = timeSeries;
    this.signalPrediction = signalPrediction;

  }

  private void buildTATool(TimeSeries timeSeries) {
    SMAIndicator ma5Indicator = new SMAIndicator(new ClosePriceIndicator(timeSeries), 5);
    SMAIndicator ma9Indicator = new SMAIndicator(new ClosePriceIndicator(timeSeries), 9);
    SMAIndicator ma20Indicator = new SMAIndicator(new ClosePriceIndicator(timeSeries), 20);
    SMAIndicator volMA50Indicator = new SMAIndicator(new VolumeIndicator(timeSeries),50);
    RSIIndicator rsiIndicator = new RSIIndicator(new ClosePriceIndicator(timeSeries), 14);
    MACDIndicator macdIndicator = new MACDIndicator(new ClosePriceIndicator(timeSeries));
    MACDHistogramIndicator histogramIndicator = new MACDHistogramIndicator(new ClosePriceIndicator(timeSeries), macdIndicator);
    candleInMA5Rule = new CandleBullishInMA(ma5Indicator);
    candleInMA9Rule = new CandleBullishInMA(ma9Indicator);
    candleInMA20Rule = new CandleBullishInMA(ma20Indicator);
    candleAboveMA5Rule = new CandleBullishAboveMA(ma5Indicator);
    candleAboveMA9Rule = new CandleBullishAboveMA(ma9Indicator);
    candleAboveMA20Rule = new CandleBullishAboveMA(ma20Indicator);
    volumeGreaterMA50 = new VolumeGreaterMA(volMA50Indicator);
    ma5AlmossMa9Rule = new MAAlmostCrossing(ma5Indicator, ma9Indicator);
    ma5AlmossMa20Rule = new MAAlmostCrossing(ma5Indicator, ma20Indicator);
    ma9AlmossMa20Rule = new MAAlmostCrossing(ma9Indicator, ma20Indicator);
    ma5crossMA9Rule = new MACrossEachOther(ma5Indicator, ma9Indicator);
    ma5crossMA20Rule = new MACrossEachOther(ma5Indicator, ma20Indicator);
    ma9crossMA20Rule = new MACrossEachOther(ma9Indicator, ma20Indicator);
    rsiBullishDivergence = new RSIBullishDivergence(rsiIndicator);
    rsiUnder30 = new RSIUnder30(rsiIndicator);
    threeBullishCandleRule = new ThreeBullishCandleRule(ma5Indicator);
    macdCrossRule = new MACDCross(macdIndicator);
    macdhIstogramBullishDivergenceRule = new MACDHIstogramBullishDivergence(histogramIndicator);
    macdLineBullishDivergenceRule = new MACDLineBullishDivergence(macdIndicator);
  }

  public boolean closeSocketAfter6h() {
    try {
      long timeToClose = startTime.toInstant().plus(6, ChronoUnit.HOURS).toEpochMilli();
      if (timeToClose <= System.currentTimeMillis()) {
        System.out.println("Close socket after 6h");
        socketClose.close();
        System.out.println("Socket close successfully");
        return true;
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return false;
  }


  public void readBarFromExchange(String code, int candleTime) {
    timeSeries = new BaseTimeSeries(code);
    timeSeries.setMaximumBarCount(100);
    buildTATool(timeSeries);
    CandlestickInterval candlestickInterval = null;
    if (candleTime == 30) {
      candlestickInterval = CandlestickInterval.HALF_HOURLY;
    } else if (candleTime == 60) {
      candlestickInterval = CandlestickInterval.HOURLY;
    } else if (candleTime == 120) {
      candlestickInterval = CandlestickInterval.TWO_HOURLY;
    } else if (candleTime == 240) {
      candlestickInterval = CandlestickInterval.FOUR_HOURLY;
    } else if (candleTime == 1440) {
      candlestickInterval = CandlestickInterval.DAILY;
    } else {
      candlestickInterval = CandlestickInterval.ONE_MINUTE;
    }
    try {
      List<Candlestick> first50Candle = clientRest.getCandlestickBars(code, candlestickInterval, 100, null, new Date().getTime());
      first50Candle.forEach(candlestick -> {
        CandleBar bar = new CandleBar(Duration.ofMinutes(candleTime),
            Instant.ofEpochMilli(candlestick.getCloseTime()).atZone(ZoneOffset.UTC),
            Decimal.valueOf(candlestick.getOpen()),
            Decimal.valueOf(candlestick.getHigh()),
            Decimal.valueOf(candlestick.getLow()),
            Decimal.valueOf(candlestick.getClose()),
            Decimal.valueOf(candlestick.getVolume()));
        timeSeries.addBar(bar);

      });
      updateSignals(candleTime);
      if(candleTime==1440) {
        signalPrediction.setCurrentPrice(timeSeries.getLastBar().getClosePrice().floatValue());
      }
    } catch (Exception e) {
      System.out.println("Error when try to get 100 bar : " + code + "-" + candleTime + " - Error : " + e.getMessage());
      e.printStackTrace();
    }

//      if (getFirst50BarSucces) {
//        System.out.println("Connect websocket : " + code + " - Interval : " + candleTime);
//        socketClose = client.onCandlestickEvent(code.toLowerCase(), candlestickInterval, new BinanceApiCallback<CandlestickEvent>() {
//          @Override
//          public void onResponse(CandlestickEvent response) {
//            if (firstResponse) {
//              System.out.println("Code " + code + " : " + response.getSymbol() + " - " + sf.format(new Date(response.getOpenTime())) + " - " + sf.format(new Date(response.getCloseTime())));
//              firstResponse = false;
//            }
//            try {
//              if (response.getBarFinal()) {
//                CandleBar bar = new CandleBar(Duration.ofMinutes(candleTime),
//                    Instant.ofEpochMilli(response.getCloseTime()).atZone(ZoneOffset.UTC),
//                    Decimal.valueOf(response.getOpen()),
//                    Decimal.valueOf(response.getHigh()),
//                    Decimal.valueOf(response.getLow()),
//                    Decimal.valueOf(response.getClose()),
//                    Decimal.valueOf(response.getVolume()));
//                timeSeries.addBar(bar);
//                updateSignals(candleTime);
//              } else {
//                CandleBar bar = (CandleBar) timeSeries.getLastBar();
//                convertResponeToBaseBar(response, bar);
//                updateSignals(candleTime);
//              }
//            } catch (Exception e) {
//              System.err.println("Web socket failed : " + code + " - Reason : " + e.getMessage());
//              try {
//                socketClose.close();
//              } catch (IOException e1) {
//                e1.printStackTrace();
//              }
//            }
//          }
//
//          @Override
//          public void onFailure(Throwable cause) {
//            System.err.println("Web socket failed : " + code + " - Reason : " + cause.getMessage());
//            try {
//              socketClose.close();
//            } catch (IOException e) {
//              e.printStackTrace();
//            }
//          }
//
//          @Override
//          public void onClose() {
//            System.out.println("Close socket");
//            try {
//              isRunning = false;
//              LunaBotApplication.deadThread.put(errorCode);
//            } catch (Exception e) {
//              e.printStackTrace();
//            }
//          }
//        });
//
//      }

  }

  private void updateSignals(int candleTime) throws ExecutionException, InterruptedException {
    List<String> signals = signalPrediction.getBullishSignal().get(String.valueOf(candleTime));
    checkCandleInMA5Signal(signals, candleInMA5Rule, candleTime);
    checkCandleInMA9Signal(signals, candleInMA9Rule, candleTime);
    checkCandleInMA20Signal(signals, candleInMA20Rule, candleTime);
    checkCandleAboveMA5Signal(signals, candleAboveMA5Rule, candleTime);
    checkCandleAboveMA9Signal(signals, candleAboveMA9Rule, candleTime);
    checkCandleAboveMA20Signal(signals, candleAboveMA20Rule, candleTime);
    checkMA5AlmostMA9(signals, ma5AlmossMa9Rule, candleTime);
    checkMA5AlmostMA20(signals, ma5AlmossMa20Rule, candleTime);
    checkMA9AlmostMA20(signals, ma9AlmossMa20Rule, candleTime);
    checkMA5CrossMA9(signals, ma5crossMA9Rule, candleTime);
    checkMA5CrossMA20(signals, ma5crossMA20Rule, candleTime);
    checkMA9CrossMA20(signals, ma9crossMA20Rule, candleTime);
    checkRSIBullishDivergence(signals, rsiBullishDivergence, candleTime);
    checkRSIUnder30(signals, rsiUnder30, candleTime);
    checkThreeBullishCandle(signals, threeBullishCandleRule, candleTime);
    checkMACDCross(signals, macdCrossRule, candleTime);
    checkMACDHistogramBullishDivergence(signals, macdhIstogramBullishDivergenceRule, candleTime);
    checkMACDSignalBullishDivergence(signals, macdLineBullishDivergenceRule, candleTime);
    checkVolumeAboveMA50Signal(signals,volumeGreaterMA50,candleTime);
  }

  private void checkMACDSignalBullishDivergence(List<String> signals, MACDLineBullishDivergence macdLineBullishDivergenceRule, int candleTime) throws ExecutionException, InterruptedException {
    if (macdLineBullishDivergenceRule.isSatisfied(timeSeries.getEndIndex(), null)) {
      if (signals.indexOf(SignalType.MACDLINE_BULLISH_DIVERGENCE.toString()) == -1) {
        signals.add(SignalType.MACDLINE_BULLISH_DIVERGENCE.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    } else {
      if (signals.indexOf(SignalType.MACDLINE_BULLISH_DIVERGENCE.toString()) != -1) {
        signals.remove(SignalType.MACDLINE_BULLISH_DIVERGENCE.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    }
  }

  private void checkMACDHistogramBullishDivergence(List<String> signals, MACDHIstogramBullishDivergence macdhIstogramBullishDivergenceRule, int candleTime) throws ExecutionException, InterruptedException {
    if (macdhIstogramBullishDivergenceRule.isSatisfied(timeSeries.getEndIndex(), null)) {
      if (signals.indexOf(SignalType.MACD_HISTOGRAM_BULLISH_DIVERGENCE.toString()) == -1) {
        signals.add(SignalType.MACD_HISTOGRAM_BULLISH_DIVERGENCE.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    } else {
      if (signals.indexOf(SignalType.MACD_HISTOGRAM_BULLISH_DIVERGENCE.toString()) != -1) {
        signals.remove(SignalType.MACD_HISTOGRAM_BULLISH_DIVERGENCE.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    }
  }

  private void checkMACDCross(List<String> signals, MACDCross macdCrossRule, int candleTime) throws ExecutionException, InterruptedException {
    if (macdCrossRule.isSatisfied(timeSeries.getEndIndex(), null)) {
      if (signals.indexOf(SignalType.MACD_CROSS.toString()) == -1) {
        signals.add(SignalType.MACD_CROSS.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    } else {
      if (signals.indexOf(SignalType.MACD_CROSS.toString()) != -1) {
        signals.remove(SignalType.MACD_CROSS.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    }
  }

  private void checkThreeBullishCandle(List<String> signals, ThreeBullishCandleRule threeBullishCandleRule, int candleTime) throws ExecutionException, InterruptedException {
    if (threeBullishCandleRule.isSatisfied(timeSeries.getEndIndex(), null)) {
      if (signals.indexOf(SignalType.THREE_BULLISH_CANDLE.toString()) == -1) {
        signals.add(SignalType.THREE_BULLISH_CANDLE.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    } else {
      if (signals.indexOf(SignalType.THREE_BULLISH_CANDLE.toString()) != -1) {
        signals.remove(SignalType.THREE_BULLISH_CANDLE.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    }
  }

  private void checkRSIBullishDivergence(List<String> signals, RSIBullishDivergence rsiBullishDivergence, int candleTime) throws ExecutionException, InterruptedException {
    if (rsiBullishDivergence.isSatisfied(timeSeries.getEndIndex(), null)) {
      if (signals.indexOf(SignalType.RSI_BULLISH_DIVERGENCE.toString()) == -1) {
        signals.add(SignalType.RSI_BULLISH_DIVERGENCE.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    } else {
      if (signals.indexOf(SignalType.RSI_BULLISH_DIVERGENCE.toString()) != -1) {
        signals.remove(SignalType.RSI_BULLISH_DIVERGENCE.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    }
  }

  private void checkRSIUnder30(List<String> signals, RSIUnder30 rsiUnder30, int candleTime) throws ExecutionException, InterruptedException {
    if (rsiUnder30.isSatisfied(timeSeries.getEndIndex(), null)) {
      if (signals.indexOf(SignalType.RSI_UNDER_30.toString()) == -1) {
        signals.add(SignalType.RSI_UNDER_30.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    } else {
      if (signals.indexOf(SignalType.RSI_UNDER_30.toString()) != -1) {
        signals.remove(SignalType.RSI_UNDER_30.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    }
  }

  private void checkMA9CrossMA20(List<String> signals, MACrossEachOther ma9crossMA20Rule, int candleTime) throws ExecutionException, InterruptedException {
    if (ma9crossMA20Rule.isSatisfied(timeSeries.getEndIndex(), null)) {
      if (signals.indexOf(SignalType.MA9_CROSS_MA20.toString()) == -1) {
        signals.add(SignalType.MA9_CROSS_MA20.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    } else {
      if (signals.indexOf(SignalType.MA9_CROSS_MA20.toString()) != -1) {
        signals.remove(SignalType.MA9_CROSS_MA20.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);
      }
    }
  }

  private void checkMA5CrossMA20(List<String> signals, MACrossEachOther ma5crossMA20Rule, int candleTime) throws ExecutionException, InterruptedException {
    if (ma5crossMA20Rule.isSatisfied(timeSeries.getEndIndex(), null)) {
      if (signals.indexOf(SignalType.MA5_CROSS_MA20.toString()) == -1) {
        signals.add(SignalType.MA5_CROSS_MA20.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    } else {
      if (signals.indexOf(SignalType.MA5_CROSS_MA20.toString()) != -1) {
        signals.remove(SignalType.MA5_CROSS_MA20.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    }
  }

  private void checkMA5CrossMA9(List<String> signals, MACrossEachOther ma5crossMA9Rule, int candleTime) throws ExecutionException, InterruptedException {
    if (ma5crossMA9Rule.isSatisfied(timeSeries.getEndIndex(), null)) {
      if (signals.indexOf(SignalType.MA5_CROSS_MA9.toString()) == -1) {
        signals.add(SignalType.MA5_CROSS_MA9.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    } else {
      if (signals.indexOf(SignalType.MA5_CROSS_MA9.toString()) != -1) {
        signals.remove(SignalType.MA5_CROSS_MA9.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    }
  }

  private void checkMA9AlmostMA20(List<String> signals, MAAlmostCrossing ma9AlmossMa20Rule, int candleTime) throws ExecutionException, InterruptedException {
    if (ma9AlmossMa20Rule.isSatisfied(timeSeries.getEndIndex(), null)) {
      if (signals.indexOf(SignalType.MA9_AlMOST_MA20.toString()) == -1) {
        signals.add(SignalType.MA9_AlMOST_MA20.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    } else {
      if (signals.indexOf(SignalType.MA9_AlMOST_MA20.toString()) != -1) {
        signals.remove(SignalType.MA9_AlMOST_MA20.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    }
  }

  private void checkMA5AlmostMA20(List<String> signals, MAAlmostCrossing ma5AlmossMa20Rule, int candleTime) throws ExecutionException, InterruptedException {
    if (ma5AlmossMa20Rule.isSatisfied(timeSeries.getEndIndex(), null)) {
      if (signals.indexOf(SignalType.MA5_AlMOST_MA20.toString()) == -1) {
        signals.add(SignalType.MA5_AlMOST_MA20.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    } else {
      if (signals.indexOf(SignalType.MA5_AlMOST_MA20.toString()) != -1) {
        signals.remove(SignalType.MA5_AlMOST_MA20.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    }
  }

  private void checkMA5AlmostMA9(List<String> signals, MAAlmostCrossing ma5AlmossMa9Rule, int candleTime) throws ExecutionException, InterruptedException {
    if (ma5AlmossMa9Rule.isSatisfied(timeSeries.getEndIndex(), null)) {
      if (signals.indexOf(SignalType.MA5_AlMOST_MA9.toString()) == -1) {
        signals.add(SignalType.MA5_AlMOST_MA9.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    } else {
      if (signals.indexOf(SignalType.MA5_AlMOST_MA9.toString()) != -1) {
        signals.remove(SignalType.MA5_AlMOST_MA9.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    }
  }

  private void checkCandleInMA20Signal(List<String> signals, CandleBullishInMA candleInMA20Rule, int candleTime) throws ExecutionException, InterruptedException {
    if (candleInMA20Rule.isSatisfied(timeSeries.getEndIndex(), null)) {
      if (signals.indexOf(SignalType.CANDLE_IN_MA20.toString()) == -1) {
        signals.add(SignalType.CANDLE_IN_MA20.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    } else {
      if (signals.indexOf(SignalType.CANDLE_IN_MA20.toString()) != -1) {
        signals.remove(SignalType.CANDLE_IN_MA20.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    }
  }

  private void checkCandleInMA9Signal(List<String> signals, CandleBullishInMA candleInMA9Rule, int candleTime) throws ExecutionException, InterruptedException {
    if (candleInMA9Rule.isSatisfied(timeSeries.getEndIndex(), null)) {
      if (signals.indexOf(SignalType.CANDLE_IN_MA9.toString()) == -1) {
        signals.add(SignalType.CANDLE_IN_MA9.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    } else {
      if (signals.indexOf(SignalType.CANDLE_IN_MA9.toString()) != -1) {
        signals.remove(SignalType.CANDLE_IN_MA9.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    }
  }

  private void checkCandleInMA5Signal(List<String> signals, CandleBullishInMA candleInMA5Rule, int candleTime) throws ExecutionException, InterruptedException {
    if (candleInMA5Rule.isSatisfied(timeSeries.getEndIndex(), null)) {
      if (signals.indexOf(SignalType.CANDLE_IN_MA5.toString()) == -1) {
        signals.add(SignalType.CANDLE_IN_MA5.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    } else {
      if (signals.indexOf(SignalType.CANDLE_IN_MA5.toString()) != -1) {
        signals.remove(SignalType.CANDLE_IN_MA5.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    }
  }

  private void checkCandleAboveMA5Signal(List<String> signals, CandleBullishAboveMA candleAboveMA5Rule, int candleTime) throws ExecutionException, InterruptedException {
    if (candleAboveMA5Rule.isSatisfied(timeSeries.getEndIndex(), null)) {
      if (signals.indexOf(SignalType.CANDLE_ABOVE_MA5.toString()) == -1) {
        signals.add(SignalType.CANDLE_ABOVE_MA5.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    } else {
      if (signals.indexOf(SignalType.CANDLE_ABOVE_MA5.toString()) != -1) {
        signals.remove(SignalType.CANDLE_ABOVE_MA5.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    }
  }

  private void checkCandleAboveMA9Signal(List<String> signals, CandleBullishAboveMA candleAboveMA9Rule, int candleTime) throws ExecutionException, InterruptedException {
    if (candleAboveMA9Rule.isSatisfied(timeSeries.getEndIndex(), null)) {
      if (signals.indexOf(SignalType.CANDLE_ABOVE_MA9.toString()) == -1) {
        signals.add(SignalType.CANDLE_ABOVE_MA9.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    } else {
      if (signals.indexOf(SignalType.CANDLE_ABOVE_MA9.toString()) != -1) {
        signals.remove(SignalType.CANDLE_ABOVE_MA9.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    }
  }

  private void checkCandleAboveMA20Signal(List<String> signals, CandleBullishAboveMA candleAboveMA20Rule, int candleTime) throws ExecutionException, InterruptedException {
    if (candleAboveMA20Rule.isSatisfied(timeSeries.getEndIndex(), null)) {
      if (signals.indexOf(SignalType.CANDLE_ABOVE_MA20.toString()) == -1) {
        signals.add(SignalType.CANDLE_ABOVE_MA20.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    } else {
      if (signals.indexOf(SignalType.CANDLE_ABOVE_MA20.toString()) != -1) {
        signals.remove(SignalType.CANDLE_ABOVE_MA20.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    }
  }

  private void checkVolumeAboveMA50Signal(List<String> signals, VolumeGreaterMA volumeGreaterMA50, int candleTime) throws ExecutionException, InterruptedException {
    if (volumeGreaterMA50.isSatisfied(timeSeries.getEndIndex(), null)) {
      if (signals.indexOf(SignalType.VOLUME_GREATER_MA50.toString()) == -1) {
        signals.add(SignalType.VOLUME_GREATER_MA50.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    } else {
      if (signals.indexOf(SignalType.VOLUME_GREATER_MA50.toString()) != -1) {
        signals.remove(SignalType.VOLUME_GREATER_MA50.toString());
        signalPrediction.getBullishSignal().put(String.valueOf(candleTime), signals);

      }
    }
  }


  private void convertResponeToBaseBar(CandlestickEvent response, CandleBar candleBar) {
    candleBar.setMaxPrice(Decimal.valueOf(response.getHigh()));
    candleBar.setMinPrice(Decimal.valueOf(response.getLow()));
    candleBar.setClosePrice(Decimal.valueOf(response.getClose()));
    candleBar.setVolume(Decimal.valueOf(response.getVolume()));
  }


}
