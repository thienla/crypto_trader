package tradingbot.model;

/**
 * Created by truongnhukhang on 11/13/18.
 */
public class SignalPredictionLearning {
  SignalPrediction signalPrediction;
  float realPriceAfter5min;
  float realPriceAfter10min;
  float realPriceAfter15min;
  float realPriceAfter30min;
  float realPriceAfter60min;
  float realPriceAfter120min;
  float realPriceAfter240min;
  float realPriceAfter480min;
  float realPriceAfter1440min;
  float realPriceAfter2480min;
  String algorithm;

  public SignalPredictionLearning() {
  }

  public SignalPredictionLearning(SignalPrediction signalPrediction) {
    this.signalPrediction = signalPrediction;
  }

  public float getRealPriceAfter5min() {
    return realPriceAfter5min;
  }

  public void setRealPriceAfter5min(float realPriceAfter5min) {
    this.realPriceAfter5min = realPriceAfter5min;
  }

  public float getRealPriceAfter10min() {
    return realPriceAfter10min;
  }

  public void setRealPriceAfter10min(float realPriceAfter10min) {
    this.realPriceAfter10min = realPriceAfter10min;
  }

  public float getRealPriceAfter15min() {
    return realPriceAfter15min;
  }

  public void setRealPriceAfter15min(float realPriceAfter15min) {
    this.realPriceAfter15min = realPriceAfter15min;
  }

  public float getRealPriceAfter30min() {
    return realPriceAfter30min;
  }

  public void setRealPriceAfter30min(float realPriceAfter30min) {
    this.realPriceAfter30min = realPriceAfter30min;
  }

  public float getRealPriceAfter60min() {
    return realPriceAfter60min;
  }

  public void setRealPriceAfter60min(float realPriceAfter60min) {
    this.realPriceAfter60min = realPriceAfter60min;
  }

  public float getRealPriceAfter120min() {
    return realPriceAfter120min;
  }

  public void setRealPriceAfter120min(float realPriceAfter120min) {
    this.realPriceAfter120min = realPriceAfter120min;
  }

  public float getRealPriceAfter240min() {
    return realPriceAfter240min;
  }

  public void setRealPriceAfter240min(float realPriceAfter240min) {
    this.realPriceAfter240min = realPriceAfter240min;
  }

  public float getRealPriceAfter480min() {
    return realPriceAfter480min;
  }

  public void setRealPriceAfter480min(float realPriceAfter480min) {
    this.realPriceAfter480min = realPriceAfter480min;
  }

  public float getRealPriceAfter1440min() {
    return realPriceAfter1440min;
  }

  public void setRealPriceAfter1440min(float realPriceAfter1440min) {
    this.realPriceAfter1440min = realPriceAfter1440min;
  }

  public float getRealPriceAfter2480min() {
    return realPriceAfter2480min;
  }

  public void setRealPriceAfter2480min(float realPriceAfter2480min) {
    this.realPriceAfter2480min = realPriceAfter2480min;
  }

  public String getAlgorithm() {
    return algorithm;
  }

  public void setAlgorithm(String algorithm) {
    this.algorithm = algorithm;
  }

  public SignalPrediction getSignalPrediction() {
    return signalPrediction;
  }

  public void setSignalPrediction(SignalPrediction signalPrediction) {
    this.signalPrediction = signalPrediction;
  }
}
