package tradingbot;

import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.MACDIndicator;
import org.ta4j.core.indicators.RSIIndicator;
import org.ta4j.core.indicators.SMAIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;

import org.ta4j.core.indicators.helpers.VolumeIndicator;
import tradingbot.indicator.MACDHistogramIndicator;

import tradingbot.rule.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by truongnhukhang on 6/18/18.
 */
public class TAWorker {
  TimeSeries timeSeries;
  CandleBullishInMA candleInMA5Rule;
  CandleBullishInMA candleInMA9Rule;
  CandleBullishInMA candleInMA20Rule;
  MAAlmostCrossing ma5AlmossMa9Rule;
  MAAlmostCrossing ma5AlmossMa20Rule;
  MACrossEachOther ma5crossMA9Rule;
  MACrossEachOther ma5crossMA20Rule;
  MACrossEachOther ma9crossMA20Rule;
  RSIBullishDivergence rsiBullishDivergence;
  ThreeBullishCandleRule threeBullishCandleRule;
  MACDCross macdCrossRule;
  MACDHIstogramBullishDivergence macdHistogramBullishDivergenceRule;
  MACDLineBullishDivergence macdLineBullishDivergenceRule;
  MAAlmostCrossing ma9AlmossMa20Rule;
  CandleBullishAboveMA candleAboveMA5Rule;
  CandleBullishAboveMA candleAboveMA9Rule;
  CandleBullishAboveMA candleAboveMA20Rule;
  RSIUnder30 rsiUnder30;
  VolumeGreaterMA volumeGreaterMA;
  
  public TAWorker(TimeSeries timeSeries) {
    this.timeSeries = timeSeries;
    SMAIndicator ma5Indicator = new SMAIndicator(new ClosePriceIndicator(timeSeries), 5);
    SMAIndicator ma9Indicator = new SMAIndicator(new ClosePriceIndicator(timeSeries), 9);
    SMAIndicator ma20Indicator = new SMAIndicator(new ClosePriceIndicator(timeSeries), 20);
    SMAIndicator vol50MAIndicator = new SMAIndicator(new VolumeIndicator(timeSeries),50);
    RSIIndicator rsiIndicator = new RSIIndicator(new ClosePriceIndicator(timeSeries), 14);
    MACDIndicator macdIndicator = new MACDIndicator(new ClosePriceIndicator(timeSeries));
    MACDHistogramIndicator histogramIndicator = new MACDHistogramIndicator(new ClosePriceIndicator(timeSeries), macdIndicator);
    candleInMA5Rule = new CandleBullishInMA(ma5Indicator);
    candleInMA9Rule = new CandleBullishInMA(ma9Indicator);
    candleInMA20Rule = new CandleBullishInMA(ma20Indicator);
    candleAboveMA5Rule = new CandleBullishAboveMA(ma5Indicator);
    candleAboveMA9Rule = new CandleBullishAboveMA(ma9Indicator);
    candleAboveMA20Rule = new CandleBullishAboveMA(ma20Indicator);
    rsiUnder30 = new RSIUnder30(rsiIndicator);
    ma5AlmossMa9Rule = new MAAlmostCrossing(ma5Indicator, ma9Indicator);
    ma5AlmossMa20Rule = new MAAlmostCrossing(ma5Indicator, ma20Indicator);
    ma9AlmossMa20Rule = new MAAlmostCrossing(ma9Indicator, ma20Indicator);
    ma5crossMA9Rule = new MACrossEachOther(ma5Indicator, ma9Indicator);
    ma5crossMA20Rule = new MACrossEachOther(ma5Indicator, ma20Indicator);
    ma9crossMA20Rule = new MACrossEachOther(ma9Indicator, ma20Indicator);
    rsiBullishDivergence = new RSIBullishDivergence(rsiIndicator);
    threeBullishCandleRule = new ThreeBullishCandleRule(ma5Indicator);
    macdCrossRule = new MACDCross(macdIndicator);
    macdHistogramBullishDivergenceRule = new MACDHIstogramBullishDivergence(histogramIndicator);
    macdLineBullishDivergenceRule = new MACDLineBullishDivergence(macdIndicator);
    volumeGreaterMA = new VolumeGreaterMA(vol50MAIndicator);
  }

  public List<String> buildSignalListFromSeriesIndex(int index) {
    List<String> signalList = new ArrayList<>();
    if(candleInMA5Rule.isSatisfied(index,null)){
      signalList.add(new String(SignalType.CANDLE_IN_MA5.toString()));
    }
    if(candleInMA9Rule.isSatisfied(index,null)){
      signalList.add(new String(SignalType.CANDLE_IN_MA9.toString()));
    }
    if(candleInMA20Rule.isSatisfied(index,null)){
      signalList.add(new String(SignalType.CANDLE_IN_MA20.toString()));
    }
    if(ma5AlmossMa9Rule.isSatisfied(index,null)){
      signalList.add(new String(SignalType.MA5_AlMOST_MA9.toString()));
    }
    if(ma5AlmossMa20Rule.isSatisfied(index,null)) {
      signalList.add(new String(SignalType.MA5_AlMOST_MA20.toString()));
    }
    if(ma9AlmossMa20Rule.isSatisfied(index,null)) {
      signalList.add(new String(SignalType.MA9_AlMOST_MA20.toString()));
    }
    if(ma5crossMA9Rule.isSatisfied(index,null)) {
      signalList.add(new String(SignalType.MA5_CROSS_MA9.toString()));
    }
    if(ma5crossMA20Rule.isSatisfied(index,null)) {
      signalList.add(new String(SignalType.MA5_CROSS_MA20.toString()));
    }
    if(ma9crossMA20Rule.isSatisfied(index,null)) {
      signalList.add(new String(SignalType.MA9_CROSS_MA20.toString()));
    }
    if(macdCrossRule.isSatisfied(index,null)) {
      signalList.add(new String(SignalType.MACD_CROSS.toString()));
    }
    if(rsiBullishDivergence.isSatisfied(index,null)) {
      signalList.add(new String(SignalType.RSI_BULLISH_DIVERGENCE.toString()));
    }
    if(macdLineBullishDivergenceRule.isSatisfied(index,null)) {
      signalList.add(new String(SignalType.MACDLINE_BULLISH_DIVERGENCE.toString()));
    }
    if(macdHistogramBullishDivergenceRule.isSatisfied(index,null)) {
      signalList.add(new String(SignalType.MACD_HISTOGRAM_BULLISH_DIVERGENCE.toString()));
    }
    if(threeBullishCandleRule.isSatisfied(index,null)) {
      signalList.add(new String(SignalType.THREE_BULLISH_CANDLE.toString()));
    }
    if(candleAboveMA5Rule.isSatisfied(index,null)) {
      signalList.add(new String(SignalType.CANDLE_ABOVE_MA5.toString()));
    }
    if(candleAboveMA9Rule.isSatisfied(index,null)) {
      signalList.add(new String(SignalType.CANDLE_ABOVE_MA9.toString()));
    }
    if(candleAboveMA20Rule.isSatisfied(index,null)) {
      signalList.add(new String(SignalType.CANDLE_ABOVE_MA20.toString()));
    }
    if(rsiUnder30.isSatisfied(index,null)) {
      signalList.add(new String(SignalType.RSI_UNDER_30.toString()));
    }
    if(volumeGreaterMA.isSatisfied(index,null)) {
      signalList.add(new String(SignalType.VOLUME_GREATER_MA50.toString()));
    }
    return signalList;
  }

}
