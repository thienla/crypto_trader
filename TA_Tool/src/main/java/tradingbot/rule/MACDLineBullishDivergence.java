package tradingbot.rule;

import org.ta4j.core.Bar;
import org.ta4j.core.Decimal;
import org.ta4j.core.TradingRecord;
import org.ta4j.core.indicators.MACDIndicator;
import org.ta4j.core.trading.rules.AbstractRule;

public class MACDLineBullishDivergence extends AbstractRule {

  MACDIndicator macdIndicator;

  public MACDLineBullishDivergence(MACDIndicator macdIndicator) {
    this.macdIndicator = macdIndicator;
  }

  @Override
  public boolean isSatisfied(int index, TradingRecord tradingRecord) {
    Decimal currentRSI = macdIndicator.getValue(index);
    Decimal previousRSI = macdIndicator.getValue(index-1);
    Bar theLowerCandle = null;
    if(currentRSI.isLessThan(0) && previousRSI.isGreaterThan(currentRSI)) {
      for (int i = 2; i < 15; i++) {
        Decimal value = macdIndicator.getValue(index-i);
        if(value.isLessThanOrEqual(currentRSI)) {
          theLowerCandle = macdIndicator.getTimeSeries().getBar(index-i);
          break;
        }
      }
    }
    if(theLowerCandle==null) {
      return false;
    }
    return theLowerCandle.getClosePrice().isGreaterThanOrEqual(macdIndicator.getTimeSeries().getBar(index).getClosePrice());
  }
}
