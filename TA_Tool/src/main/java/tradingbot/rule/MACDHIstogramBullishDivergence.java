package tradingbot.rule;

import org.ta4j.core.Bar;
import org.ta4j.core.Decimal;
import org.ta4j.core.TradingRecord;
import org.ta4j.core.trading.rules.AbstractRule;
import tradingbot.indicator.MACDHistogramIndicator;

/**
 * Created by truongnhukhang on 6/3/18.
 */
public class MACDHIstogramBullishDivergence extends AbstractRule{

  MACDHistogramIndicator macdHistogramIndicator;

  public MACDHIstogramBullishDivergence(MACDHistogramIndicator macdHistogramIndicator) {
    this.macdHistogramIndicator = macdHistogramIndicator;
  }


  @Override
  public boolean isSatisfied(int index, TradingRecord tradingRecord) {
    Decimal currentRSI = macdHistogramIndicator.getValue(index);
    Decimal previousRSI = macdHistogramIndicator.getValue(index-1);
    Bar theLowerCandle = null;
    if(currentRSI.isLessThan(0) && previousRSI.isGreaterThan(currentRSI)) {
      for (int i = 2; i < 15; i++) {
        Decimal value = macdHistogramIndicator.getValue(index-i);
        if(value.isLessThanOrEqual(currentRSI)) {
          theLowerCandle = macdHistogramIndicator.getTimeSeries().getBar(index-i);
          break;
        }
      }
    }
    if(theLowerCandle==null) {
      return false;
    }
    return theLowerCandle.getClosePrice().isGreaterThanOrEqual(macdHistogramIndicator.getTimeSeries().getBar(index).getClosePrice());
  }

}
