package tradingbot.rule;

import org.ta4j.core.Decimal;
import org.ta4j.core.TradingRecord;
import org.ta4j.core.indicators.SMAIndicator;
import org.ta4j.core.trading.rules.AbstractRule;

/**
 * Created by truongnhukhang on 10/26/18.
 */
public class VolumeGreaterMA extends AbstractRule {

  SMAIndicator smaIndicator;

  public VolumeGreaterMA(SMAIndicator smaIndicator) {
    this.smaIndicator = smaIndicator;
  }

  @Override
  public boolean isSatisfied(int index, TradingRecord tradingRecord) {
    Decimal vol = smaIndicator.getTimeSeries().getBar(index).getVolume();
    return smaIndicator.getValue(index).isLessThan(vol);
  }
}
