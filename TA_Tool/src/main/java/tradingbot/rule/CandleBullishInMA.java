package tradingbot.rule;

import org.ta4j.core.Bar;
import org.ta4j.core.Decimal;
import org.ta4j.core.TradingRecord;
import org.ta4j.core.indicators.SMAIndicator;
import org.ta4j.core.trading.rules.AbstractRule;

public class CandleBullishInMA  extends AbstractRule {

  SMAIndicator smaIndicator;

  public CandleBullishInMA(SMAIndicator smaIndicator) {
    this.smaIndicator = smaIndicator;
  }

  @Override
  public boolean isSatisfied(int index, TradingRecord tradingRecord) {
    Decimal smaValue = smaIndicator.getValue(index);
    Bar candleBar = smaIndicator.getTimeSeries().getBar(index);
    return smaIndicator.getTimeSeries().getBar(index-1).getClosePrice().isLessThanOrEqual(smaIndicator.getValue(index-1)) &&
        smaValue.isGreaterThan(candleBar.getOpenPrice()) &&
        smaValue.isLessThan(candleBar.getClosePrice());
  }
}
