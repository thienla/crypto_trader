package tradingbot.dto;

import tradingbot.SignalType;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Created by truongnhukhang on 7/27/18.
 */
public class GroupCandleDto {
  String symbol;
  long closeTime;
  float price;
  Map<String,List<String>> bullishSignal = new ConcurrentHashMap<>();
  Map<String,Double> recentBuyVolume;
  Map<String,Double> recentSellVolume;
  Map<String,Double> recentBuySymbolVolume;
  Map<String,Double> recentSellSymbolVolume;
  List<Float> btcMA5_4h;
  List<Float> btcMA5_1d;
  float priceAfter5min;
  float priceAfter10min;
  float priceAfter15min;
  float priceAfter30min;
  float priceAfter60min;
  float priceAfter120min;
  float priceAfter240min;
  float priceAfter480min;
  float priceAfter1440min;
  float priceAfter2480min;
  float btcPrice5mAgo;
  float btcPrice10mAgo;
  float btcPrice15mAgo;
  float btcPrice30mAgo;
  float btcPrice60mAgo;
  float btcPrice120mAgo;
  float btcPrice240mAgo;
  float btcPrice480mAgo;
  float btcPrice1440mAgo;
  float btcPrice2480mAgo;
  float btcPrice10080mAgo;

  public String getSymbol() {
    return symbol;
  }

  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }

  public Map<String, List<String>> getBullishSignal() {
    return bullishSignal;
  }

  public void setBullishSignal(Map<String, List<String>> bullishSignal) {
    this.bullishSignal = bullishSignal;
  }

  public Map<String, Double> getRecentBuyVolume() {
    return recentBuyVolume;
  }

  public void setRecentBuyVolume(Map<String, Double> recentBuyVolume) {
    this.recentBuyVolume = recentBuyVolume;
  }

  public Map<String, Double> getRecentSellVolume() {
    return recentSellVolume;
  }

  public void setRecentSellVolume(Map<String, Double> recentSellVolume) {
    this.recentSellVolume = recentSellVolume;
  }

  public Map<String, Double> getRecentBuySymbolVolume() {
    return recentBuySymbolVolume;
  }

  public void setRecentBuySymbolVolume(Map<String, Double> recentBuySymbolVolume) {
    this.recentBuySymbolVolume = recentBuySymbolVolume;
  }

  public Map<String, Double> getRecentSellSymbolVolume() {
    return recentSellSymbolVolume;
  }

  public void setRecentSellSymbolVolume(Map<String, Double> recentSellSymbolVolume) {
    this.recentSellSymbolVolume = recentSellSymbolVolume;
  }

  public long getCloseTime() {
    return closeTime;
  }

  public void setCloseTime(long closeTime) {
    this.closeTime = closeTime;
  }

  public float getPriceAfter5min() {
    return priceAfter5min;
  }

  public void setPriceAfter5min(float priceAfter5min) {
    this.priceAfter5min = priceAfter5min;
  }

  public float getPriceAfter10min() {
    return priceAfter10min;
  }

  public void setPriceAfter10min(float priceAfter10min) {
    this.priceAfter10min = priceAfter10min;
  }

  public float getPriceAfter15min() {
    return priceAfter15min;
  }

  public void setPriceAfter15min(float priceAfter15min) {
    this.priceAfter15min = priceAfter15min;
  }

  public float getPriceAfter30min() {
    return priceAfter30min;
  }

  public void setPriceAfter30min(float priceAfter30min) {
    this.priceAfter30min = priceAfter30min;
  }

  public float getPriceAfter60min() {
    return priceAfter60min;
  }

  public void setPriceAfter60min(float priceAfter60min) {
    this.priceAfter60min = priceAfter60min;
  }

  public float getPriceAfter120min() {
    return priceAfter120min;
  }

  public void setPriceAfter120min(float priceAfter120min) {
    this.priceAfter120min = priceAfter120min;
  }

  public float getPriceAfter240min() {
    return priceAfter240min;
  }

  public void setPriceAfter240min(float priceAfter240min) {
    this.priceAfter240min = priceAfter240min;
  }

  public float getPriceAfter480min() {
    return priceAfter480min;
  }

  public void setPriceAfter480min(float priceAfter480min) {
    this.priceAfter480min = priceAfter480min;
  }

  public float getPriceAfter1440min() {
    return priceAfter1440min;
  }

  public void setPriceAfter1440min(float priceAfter1440min) {
    this.priceAfter1440min = priceAfter1440min;
  }

  public float getPriceAfter2480min() {
    return priceAfter2480min;
  }

  public void setPriceAfter2480min(float priceAfter2480min) {
    this.priceAfter2480min = priceAfter2480min;
  }

  public float getBtcPrice5mAgo() {
    return btcPrice5mAgo;
  }

  public void setBtcPrice5mAgo(float btcPrice5mAgo) {
    this.btcPrice5mAgo = btcPrice5mAgo;
  }

  public float getBtcPrice10mAgo() {
    return btcPrice10mAgo;
  }

  public void setBtcPrice10mAgo(float btcPrice10mAgo) {
    this.btcPrice10mAgo = btcPrice10mAgo;
  }

  public float getBtcPrice15mAgo() {
    return btcPrice15mAgo;
  }

  public void setBtcPrice15mAgo(float btcPrice15mAgo) {
    this.btcPrice15mAgo = btcPrice15mAgo;
  }

  public float getBtcPrice30mAgo() {
    return btcPrice30mAgo;
  }

  public void setBtcPrice30mAgo(float btcPrice30mAgo) {
    this.btcPrice30mAgo = btcPrice30mAgo;
  }

  public float getBtcPrice60mAgo() {
    return btcPrice60mAgo;
  }

  public void setBtcPrice60mAgo(float btcPrice60mAgo) {
    this.btcPrice60mAgo = btcPrice60mAgo;
  }

  public float getBtcPrice120mAgo() {
    return btcPrice120mAgo;
  }

  public void setBtcPrice120mAgo(float btcPrice120mAgo) {
    this.btcPrice120mAgo = btcPrice120mAgo;
  }

  public float getBtcPrice240mAgo() {
    return btcPrice240mAgo;
  }

  public void setBtcPrice240mAgo(float btcPrice240mAgo) {
    this.btcPrice240mAgo = btcPrice240mAgo;
  }

  public float getBtcPrice480mAgo() {
    return btcPrice480mAgo;
  }

  public void setBtcPrice480mAgo(float btcPrice480mAgo) {
    this.btcPrice480mAgo = btcPrice480mAgo;
  }

  public float getBtcPrice1440mAgo() {
    return btcPrice1440mAgo;
  }

  public void setBtcPrice1440mAgo(float btcPrice1440mAgo) {
    this.btcPrice1440mAgo = btcPrice1440mAgo;
  }

  public float getBtcPrice2480mAgo() {
    return btcPrice2480mAgo;
  }

  public void setBtcPrice2480mAgo(float btcPrice2480mAgo) {
    this.btcPrice2480mAgo = btcPrice2480mAgo;
  }

  public float getBtcPrice10080mAgo() {
    return btcPrice10080mAgo;
  }

  public void setBtcPrice10080mAgo(float btcPrice10080mAgo) {
    this.btcPrice10080mAgo = btcPrice10080mAgo;
  }

  public List<Float> getBtcMA5_4h() {
    return btcMA5_4h;
  }

  public void setBtcMA5_4h(List<Float> btcMA5_4h) {
    this.btcMA5_4h = btcMA5_4h;
  }

  public List<Float> getBtcMA5_1d() {
    return btcMA5_1d;
  }

  public void setBtcMA5_1d(List<Float> btcMA5_1d) {
    this.btcMA5_1d = btcMA5_1d;
  }

  public float getPrice() {
    return price;
  }

  public void setPrice(float price) {
    this.price = price;
  }

}
