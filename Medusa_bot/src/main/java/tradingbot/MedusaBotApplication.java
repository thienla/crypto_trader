package tradingbot;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.general.SymbolInfo;
import com.binance.api.client.domain.market.CandlestickInterval;
import org.neo4j.ogm.config.ClasspathConfigurationSource;
import org.neo4j.ogm.config.ConfigurationSource;
import org.neo4j.ogm.session.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.neo4j.Neo4jDataAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.transaction.Neo4jTransactionManager;
import org.springframework.scheduling.annotation.EnableAsync;
import org.ta4j.core.BaseTimeSeries;
import org.ta4j.core.TimeSeries;
import tradingbot.dto.GroupCandleDto;
import tradingbot.dto.PredictionDto;
import tradingbot.model.*;
import tradingbot.reader.BinanceBackTestReader;
import tradingbot.reader.ExchangeReader;
import tradingbot.repository.CandleRepository;
import tradingbot.repository.GroupCandleRepository;
import tradingbot.repository.SignalRepository;
import tradingbot.service.GroupCandleService;
import tradingbot.service.PredictionService;
import tradingbot.worker.DataSender;
import tradingbot.worker.GroupCandleCrawler;
import tradingbot.worker.Neo4jDataSender;
import tradingbot.worker.TAWorker;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@SpringBootApplication(exclude = Neo4jDataAutoConfiguration.class)
@EnableAsync
public class MedusaBotApplication implements CommandLineRunner {

  @Autowired
  SignalRepository signalRepository;

  @Autowired
  GroupCandleService groupCandleService;

  @Autowired
  PredictionService predictionService;

  @Autowired
  GroupCandleRepository groupCandleRepository;

  @Autowired
  DataSender neo4jDataSender;

  @Autowired
  GroupCandleCrawler groupCandleCrawler;

  public static void main(String[] args) {
    SpringApplication springApplication =
        new SpringApplicationBuilder()
            .sources(MedusaBotApplication.class)
            .build();

    springApplication.run(MedusaBotApplication.class, args);
  }

  @Override
  public void run(String... args) throws Exception {
    System.out.println("Start server");
    updateSignalList();
//    groupCandleCrawler.saveDataFromJson();
//    groupCandleCrawler.crawFuturePrice();
//    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
//    long startTime = simpleDateFormat.parse("22/12/2017").getTime();
//    long startTime = simpleDateFormat.parse("9/2/2018").getTime();
//    long endTime = simpleDateFormat.parse("21/06/2018").getTime();
//    long endTime = simpleDateFormat.parse("10/2/2018").getTime();
//    groupCandleCrawler.crawAllSymbol(startTime,endTime,false,true);
//    Date start = new Date();
//    List<String> signals = new ArrayList<>();
//    String[] strings = {"Histogram Bull divergence","MACD Bull cross","MACDLine Bull divergence","Candle in MA5","MA5 ALMOST_CROSS MA9","Rsi Bull divergence"};
//    signals.addAll(Arrays.asList(strings));
//    List<String> fullSignals = new ArrayList<>();
//    fullSignals.addAll(Stream.of(SignalType.values()).map(signalType -> signalType.toString()).collect(Collectors.toList()));
//    GroupCandleFilterCondition groupCandleFilterCondition = new GroupCandleFilterCondition();
//    groupCandleFilterCondition.setCurrentPrice((float) 0.05);
//    Map<Integer,Integer> intervalMap = new HashMap<>();
//    intervalMap.put(30,1);
//    intervalMap.put(60,2);
//    intervalMap.put(120,3);
//    intervalMap.put(240,4);
//    intervalMap.put(1440,5);
//    Map<Integer,List<String>> signalMap = new HashMap<>();
//    signalMap.put(30,signals);
//    signalMap.put(60,signals);
//    signalMap.put(120,signals);
//    signalMap.put(240,signals);
//    signalMap.put(1440,signals);
//    groupCandleFilterCondition.setOpentime(Long.valueOf("1529625600000"));
//    groupCandleFilterCondition.setScoreByInterval(intervalMap);
//    groupCandleFilterCondition.setSignalListByInterval(signalMap);
//    PredictionDto predictionDto = predictionService.buildPredictionFrom(groupCandleFilterCondition,20,10);
//    System.out.println(predictionDto.getCurrentPrice());
//    System.out.println(predictionDto.getPriceAfter5min());
//    List<GroupCandleDto> groupCandlePage = predictionDto.getGroupCandleDtos();
//    groupCandlePage.stream().forEach(groupCandleId -> {
//      groupCandleId.getBullishSignal().forEach((key,value) -> {
//        System.out.println(key + " : " + value.stream().collect(Collectors.joining(",")));
//      });
//    });
//    Date end = new Date();
//    System.out.println("Time : " + (end.getTime()-start.getTime())/60000);
  }


  private void updateSignalList() {
    System.out.println("Update Signal list");
    Iterable<Signal> signals = signalRepository.findAll();
    if (signals.iterator().hasNext()) {
      List<Signal> signalTypeList = Stream.of(SignalType.values()).map(signalType -> new Signal(signalType.toString())).collect(Collectors.toList());
      List<Signal> signalList = new ArrayList<>();
      signals.forEach(signalList::add);
      signalTypeList = signalTypeList.stream().filter(signal -> signalList.stream().noneMatch(signal1 -> signal1.getType().equals(signal.getType()))).collect(Collectors.toList());
      if (signalTypeList.size() > 0) {
        signalRepository.saveAll(signalTypeList);
      }
    } else {
      List<Signal> signalTypeList = Stream.of(SignalType.values()).map(signalType -> new Signal(signalType.toString())).collect(Collectors.toList());
      signalRepository.saveAll(signalTypeList);
    }
  }
}
