package tradingbot.model;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;

import java.util.Objects;

/**
 * Created by truongnhukhang on 6/8/18.
 */
@NodeEntity
public class Signal {
  @Id
  @GeneratedValue
  Long id;
  String type;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Signal(String type) {
    this.type = type;
  }

  public Signal() {
  }



}
