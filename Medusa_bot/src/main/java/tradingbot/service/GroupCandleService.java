package tradingbot.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import tradingbot.dto.GroupCandleDto;
import tradingbot.model.GroupCandle;
import tradingbot.model.GroupCandleFilterCondition;

import java.util.List;

public interface GroupCandleService {
  public List<GroupCandleDto> findGroupCandleBySimilarSignals(GroupCandleFilterCondition groupCandleFilterCondition, Integer limit , Integer skip) ;
  public GroupCandleDto findGroupCandleBySymbolAndOpentime(String symbol,long openTime);
  public List<GroupCandle> findGroupCandleWithFuturePriceEqualZero();
}
