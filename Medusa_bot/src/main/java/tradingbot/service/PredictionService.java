package tradingbot.service;

import tradingbot.dto.GroupCandleDto;
import tradingbot.dto.PredictionDto;
import tradingbot.model.GroupCandleFilterCondition;

import java.util.List;

public interface PredictionService {
  PredictionDto buildPredictionFrom(GroupCandleFilterCondition groupCandleFilterCondition, Integer limit , Integer skip);
}
