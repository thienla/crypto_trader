package tradingbot.worker;

import tradingbot.model.Candle;
import tradingbot.model.GroupCandle;

import java.util.List;

public interface DataSender {
  void sendCandles(List<Candle> candles);
  void sendGroupCandles(List<GroupCandle> candles);
}
