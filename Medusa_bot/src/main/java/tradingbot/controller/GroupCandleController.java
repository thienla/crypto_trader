package tradingbot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tradingbot.dto.GroupCandleDto;
import tradingbot.dto.PredictionDto;
import tradingbot.model.GroupCandle;
import tradingbot.model.GroupCandleFilterCondition;
import tradingbot.service.GroupCandleService;
import tradingbot.service.PredictionService;
import tradingbot.worker.GroupCandleCrawler;

import java.util.List;

@Controller
public class GroupCandleController {

  @Autowired
  GroupCandleService groupCandleService;

  @Autowired
  PredictionService predictionService;

  @Autowired
  GroupCandleCrawler groupCandleCrawler;

  @CrossOrigin(origins = {"*"})
  @RequestMapping(value = "/groupcandles",consumes = "application/json",method = RequestMethod.POST)
  public @ResponseBody
  PredictionDto findGroupCandlesBySignals(
      @RequestBody GroupCandleFilterCondition groupCandleFilterCondition) {
    return predictionService.buildPredictionFrom(groupCandleFilterCondition,20,10);
  }


  @CrossOrigin(origins = {"*"})
  @RequestMapping(value = "/groupcandles/{startTime}/{endTime}",method = RequestMethod.PUT)
  public @ResponseBody String crawGroupCandleForAllSymbol(@PathVariable long startTime,@PathVariable long endTime,
                                                          @RequestParam(required = false,defaultValue = "false") boolean ignoreFuturePrice,@RequestParam(required = false,defaultValue = "false") boolean ignoreExistedData) {
    groupCandleCrawler.crawAllSymbol(startTime,endTime,ignoreExistedData,ignoreFuturePrice);
    return "OK";
  }

  @CrossOrigin(origins = {"*"})
  @RequestMapping(value = "/groupcandles/futureprice",method = RequestMethod.PUT)
  public @ResponseBody String crawFuturePrice() {
    groupCandleCrawler.crawFuturePrice();
    return "OK";
  }

  @CrossOrigin(origins = {"*"})
  @RequestMapping(value = "/groupcandle/{symbol}/{opentime}",method = RequestMethod.GET)
  public @ResponseBody GroupCandleDto getGroupCandle(@PathVariable String symbol, @PathVariable long opentime) {
    return groupCandleService.findGroupCandleBySymbolAndOpentime(symbol,opentime);
  }
}
