package tradingbot.reader;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.market.Candlestick;
import com.binance.api.client.domain.market.CandlestickInterval;
import org.ta4j.core.Bar;
import org.ta4j.core.BaseBar;
import org.ta4j.core.Decimal;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.MACDIndicator;
import org.ta4j.core.indicators.RSIIndicator;
import org.ta4j.core.indicators.SMAIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import tradingbot.SignalType;
import tradingbot.indicator.MACDHistogramIndicator;
import tradingbot.model.Candle;
import tradingbot.model.CandleBar;
import tradingbot.model.Signal;
import tradingbot.rule.*;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by truongnhukhang on 3/22/18.
 */
public class BinanceBackTestReader implements ExchangeReader {

  BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance("API-KEY", "SECRET");
  BinanceApiRestClient client = factory.newRestClient();
  TimeSeries timeSeries;
  List<Candlestick> historyCandle = new ArrayList<>();


  public BinanceBackTestReader(TimeSeries timeSeries) {
    this.timeSeries = timeSeries;

  }

  @Override
  public List<Candle> readCandleFromExchange(String code, int candleTime, long startTime, long endTime) {
    CandlestickInterval candlestickInterval = null;
    List<Candle> candles = new ArrayList<>();
    if (candleTime == 30) {
      candlestickInterval = CandlestickInterval.HALF_HOURLY;
    } else if (candleTime == 60) {
      candlestickInterval = CandlestickInterval.HOURLY;
    } else if (candleTime == 120) {
      candlestickInterval = CandlestickInterval.TWO_HOURLY;
    } else if (candleTime == 240) {
      candlestickInterval = CandlestickInterval.FOUR_HOURLY;
    } else if (candleTime == 1440) {
      candlestickInterval = CandlestickInterval.DAILY;
    } else {
      candlestickInterval = CandlestickInterval.ONE_MINUTE;
    }
    System.out.println("Start get data : " + code + " _ " + candleTime);
    System.out.println("start : " + startTime);
    System.out.println("end : " + endTime);
    Date startDate = new Date(startTime);
    Date endDate = new Date(endTime);
    int period = candleTime * 500;
    if(startDate.toInstant().plus(period,ChronoUnit.MINUTES).compareTo(endDate.toInstant())>=0) {
      List<Candlestick> candlesticks = client.getCandlestickBars(code,
          candlestickInterval, null, startTime, endTime
      );
      historyCandle.addAll(candlesticks);
    } else {
      for (Instant i = startDate.toInstant(); i .compareTo(endDate.toInstant()) < 0; i = i.plus(period,ChronoUnit.MINUTES)) {
        Instant instantEndTimeOfCandle = null;
        if(endDate.toInstant().minus(period,ChronoUnit.MINUTES).compareTo(i)>0) {
          instantEndTimeOfCandle = i.plus(period,ChronoUnit.MINUTES);
        } else {
          instantEndTimeOfCandle = endDate.toInstant();
        }
        System.out.println("Start get data at : " + i.toEpochMilli() + "-" + instantEndTimeOfCandle.toEpochMilli());
        List<Candlestick> candlesticks = client.getCandlestickBars(code,
            candlestickInterval, 500, i.toEpochMilli(), instantEndTimeOfCandle.toEpochMilli()
        );
        historyCandle.addAll(candlesticks);
      }
    }

    historyCandle = historyCandle.stream().filter(distinctByKey(candlestick -> candlestick)).collect(Collectors.toList());
    historyCandle.forEach(candlestick -> {
      CandleBar bar = new CandleBar(Duration.ofMinutes(candleTime),
          Instant.ofEpochMilli(candlestick.getCloseTime()).atZone(ZoneOffset.UTC),
          Decimal.valueOf(candlestick.getOpen()),
          Decimal.valueOf(candlestick.getHigh()),
          Decimal.valueOf(candlestick.getLow()),
          Decimal.valueOf(candlestick.getClose()),
          Decimal.valueOf(candlestick.getVolume()));
      try {

        timeSeries.addBar(bar);
      } catch (IllegalArgumentException e) {
        System.out.println(timeSeries.getLastBar().getBeginTime() + " _ " + timeSeries.getLastBar().getEndTime());
        System.out.println(bar.getBeginTime() + " _ " + bar.getEndTime());
      }
    });

    for (int i = 0; i <= timeSeries.getEndIndex(); i++) {
      Candlestick candlestick = historyCandle.get(i);
      Candle candle = new Candle();
      candle.setOpen(Float.parseFloat(candlestick.getOpen()));
      candle.setClose(Float.parseFloat(candlestick.getClose()));
      candle.setLowest(Float.parseFloat(candlestick.getLow()));
      candle.setHighest(Float.parseFloat(candlestick.getHigh()));
      candle.setOpenTime(timeSeries.getBar(i).getBeginTime().toInstant().toEpochMilli());
      candle.setCloseTime(timeSeries.getBar(i).getEndTime().toInstant().toEpochMilli());
      candle.setVolume(Float.parseFloat(candlestick.getVolume()));
      candle.setVolume(Float.parseFloat(candlestick.getVolume()));
      candle.setInterval(candleTime);
      candle.setBullish(timeSeries.getBar(i).isBullish());
      candle.setSymbol(code);
      candles.add(candle);
    }
    return candles;
  }
  public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor)
  {
    Map<Object, Boolean> map = new ConcurrentHashMap<>();
    return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
  }


}
